<?php
session_start();
//Lấy địa chỉ MAC của AP và user
$_SESSION["id"] = $_GET["id"];
$_SESSION["ap"] = $_GET["ap"];
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/splash.jpg" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="custom/app.css">
    <title>Bach Gia Lac WIFI</title>
</head>

<script>
    function validate() {
        var vnf_regex = /((0)+([0-9]{9})\b)/g;
        var x = document.getElementById('phone').value;
        if (!vnf_regex.test(x)) {
            alert("Số điện thoại chưa chính xác!");
            return false
        }
        return true
    }
</script>
<body>
<div class="wraper">
    <img src="custom/banner.jpg">
    <div class="content">
        <h1>Vui lòng điền các thông tin để truy cập internet!</h1>
        <div class="form-wrap">
            <form action="connecting.php" method="post" onsubmit="return validate()">
                <input type="text" name="name" value="" placeholder="Tên*" required>
                <br>
                <input type="number" id="phone" name="phone" value="" placeholder="Điện thoại*" required>
                <br>
                <input type="email" name="email" value="" placeholder="Email">
                <br>
                <input type="text" name="address" value="" placeholder="Địa chỉ">
                <br>
                <input type="submit" value="Tiếp tục">
                <p class="note">Các mục có dấu * là bắt buộc!</p>
            </form>
        </div>
        <hr>
    </div>
</div>
</body>

</html>