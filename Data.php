<?php
require_once 'Database.php';

class Data
{
    private static $table = "data";
    private static $db;

    public function __construct()
    {
        self::$db = Database::getDb();
        if (!mysqli_query(self::$db, "DESCRIBE `$this->table`")) {
            $sql = "CREATE TABLE IF NOT EXISTS `data`
                ( `id` INT NOT NULL AUTO_INCREMENT , 
                `name` TEXT NOT NULL , `phone` TEXT NOT NULL , 
                `mail` TEXT NULL DEFAULT NULL ,
                `address` TEXt NULL DEFAULT NULL , 
                `user_agent` TEXT NOT NULL , 
                `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                PRIMARY KEY (`id`)) ENGINE = InnoDB;";
            mysqli_query(self::$db, $sql);
        }
    }

    public static function insert($name, $phone, $email, $address, $ug)
    {
        $sql = "INSERT INTO `data` 
                (`id`, `name`, `phone`, `mail`, `address`, `user_agent`, `time`) 
                VALUES 
                (NULL, '$name', '$phone', '$email', '$address', '$ug', CURRENT_TIMESTAMP);";
        mysqli_query(self::$db, $sql);
    }


}