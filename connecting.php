<?php
require_once 'Data.php';
session_start();

$db = Database::getDb();

$mac = $_SESSION["id"];
$ap = $_SESSION["ap"];

require __DIR__ . '/vendor/autoload.php';

$duration = 30; //Thời gian cấp quyền sử dụng 
$site_id = ''; //Site ID  (https://1.1.1.1:8443/manage/site/<site_ID>/devices/1/50)

$controlleruser = ''; // Tài khoản đăng nhập controller
$controllerpassword = ''; // Mật khẩu đăng nhập controller
$controllerurl = "https://connect.fpt.net:8443"; // địa chỉ của controller
$controllerversion = '5.12.35'; // phiên bản của controller
$debug = false; //

//Khởi tạo đối tượng connection
$unifi_connection = new UniFi_API\Client($controlleruser, $controllerpassword, $controllerurl, $site_id, $controllerversion);
$set_debug_mode = $unifi_connection->set_debug($debug);
//Đăng nhập vào controller
$loginresults = $unifi_connection->login();

//Kiểm tra và xác thực thông tin
if ($name != "" and $email != ""){
    Data::insert($name, $phone, $email, $address, $ug);
    // Thực hiện chấp nhận cho guest truy cập mạng
    $auth_result = $unifi_connection->authorize_guest($mac, $duration, $up = null, $down = null, $MBytes = null, $ap);
}else{
    header("Location: http://wifi.imus.vn/guest/$site_id/");
}


$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$address = $_POST['address'];
$ug = $_SERVER['HTTP_USER_AGENT'];
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="refresh" content="5">
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="5;url=https://www.google.com/"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/splash.jpg" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="custom/app.css">
    <title>Imusnano shop WIFI</title>
</head>

<body>
<div class="wraper">
    <img src="custom/banner.jpg">
    <div class="content">
        <h1>Vui lòng đợi trong giây lát!</h1>
        <hr>
    </div>
</div>
</body>

</html>