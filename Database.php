<?php

class Database
{
    private static $db = null;

    public static function getDb()
    {
        if (!self::$db) {
            self::$db = mysqli_connect('localhost', 'phpmyadmin', '1', 'demo');
        }
        return self::$db;
    }
}